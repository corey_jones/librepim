#ifndef LPIMABSTRACTTASKPROVIDER_H
#define LPIMABSTRACTTASKPROVIDER_H

#include "librepimdomain_global.h"
#include "lpimtask.h"

#include <QSharedPointer>
//#include <QDateTime>
//#include <QVector>

LPIM_BEGIN_NAMESPACE
class LIBREPIMDOMAINSHARED_EXPORT LpimAbstractTaskProvider :
    public QObject
{
    Q_OBJECT
public:
    LpimAbstractTaskProvider(QObject *parent = 0);
    virtual ~LpimAbstractTaskProvider() = 0;

    virtual LpimTask* create() = 0;

    virtual void fetch(const QDateTime& from, const QDateTime& to,
               QVector<LpimTaskSharedPtr>& tasks) = 0;

    virtual void save() = 0;

// TODO: make it pure
    virtual void getLastError() const /*= 0*/;

signals:
    void changed();
};

inline LpimAbstractTaskProvider::~LpimAbstractTaskProvider()
{
}

LPIM_END_NAMESPACE

#endif // LPIMABSTRACTTASKPROVIDER_H
