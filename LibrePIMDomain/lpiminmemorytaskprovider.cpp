#include "lpiminmemorytaskprovider.h"

#include <QVector>
#include <QDateTime>
#include <QDebug>

LPIM_BEGIN_NAMESPACE

class LpimInMemoryTaskProviderImp
{
public:
    QVector<LpimTaskSharedPtr> tasks;
private:
};

LpimInMemoryTaskProvider::LpimInMemoryTaskProvider(QObject *parent) :
    LpimAbstractTaskProvider(parent),
    mImpl(new LpimInMemoryTaskProviderImp)
{
}

LpimInMemoryTaskProvider::~LpimInMemoryTaskProvider()
{
    delete mImpl;
    mImpl = 0;
}

LpimTask*
LpimInMemoryTaskProvider::create()
{
    LpimTaskSharedPtr task =
            QSharedPointer<LpimTask>::create();
//    LpimTask task;
    mImpl->tasks.append(task);
    QObject::connect(mImpl->tasks.last().data(), SIGNAL(changed(LpimTask&)),
                     this, SLOT(onTaskChanged(LpimTask&)));
    return mImpl->tasks.last().data();
}

void
LpimInMemoryTaskProvider::fetch(
        const QDateTime &from,
        const QDateTime &to,
        QVector<LpimTaskSharedPtr> &tasks)
{
    tasks.clear();
    for (int i(0); i < mImpl->tasks.size(); ++i)
    {
        LpimTaskSharedPtr task = mImpl->tasks[i];
        if (from <= task->startTime() &&
                task->endTime() < to)
        {
            tasks.append(task);
        }
    }
}

void
LpimInMemoryTaskProvider::save()
{
    // nothing to do
    // TODO: check whether there are changes?
    emit changed();
}

void LpimInMemoryTaskProvider::onTaskChanged(LpimTask &t)
{
    qDebug() << "LpimInMemoryTaskProvider::onTaskChanged: " << t.startTime();
    save();
}

LPIM_END_NAMESPACE
