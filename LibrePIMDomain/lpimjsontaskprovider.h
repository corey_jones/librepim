#ifndef LPIMJSONTASKPROVIDER_H
#define LPIMJSONTASKPROVIDER_H

#include <QVector>
#include <QSharedPointer>

#include "lpimabstracttaskprovider.h"
#include "librepimdomain_global.h"

LPIM_BEGIN_NAMESPACE

class LIBREPIMDOMAINSHARED_EXPORT LpimJsonTaskProvider :
    public LpimAbstractTaskProvider
{
    Q_OBJECT
public:
    explicit LpimJsonTaskProvider(const QString &, QObject *parent = 0);

    virtual ~LpimJsonTaskProvider();
    virtual LpimTask* create();
    virtual void fetch(const QDateTime& from, const QDateTime& to,
               QVector<LpimTaskSharedPtr>& tasks);
    virtual void save();

signals:

public slots:
private:
    QString filename;
    QVector<LpimTaskSharedPtr> tasks;
};

LPIM_END_NAMESPACE

#endif // LPIMJSONTASKPROVIDER_H
