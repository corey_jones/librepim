#ifndef LPIMTASK_H
#define LPIMTASK_H

#include "librepimdomain_global.h"

#include <QObject>
#include <QDateTime>
#include <QSharedPointer>

LPIM_BEGIN_NAMESPACE
class LpimTaskPrivate;
LPIM_END_NAMESPACE

// TODO: task move method:

LPIM_BEGIN_NAMESPACE
class LpimTask;
typedef QSharedPointer<LpimTask> LpimTaskSharedPtr;

class LIBREPIMDOMAINSHARED_EXPORT LpimTask : public QObject
{
    Q_OBJECT
public:
    explicit LpimTask(QObject *parent = 0);

    LpimTask(const LpimTask& other);
    virtual ~LpimTask();
    LpimTask &operator=(const LpimTask& other);

    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QDateTime startTime READ startTime WRITE setStartTime NOTIFY startTimeChanged)
    Q_PROPERTY(QDateTime endTime READ endTime WRITE setEndTime NOTIFY endTimeChanged)

    Q_INVOKABLE void move(QDateTime newStartTime);

    QString title() const;

    QDateTime startTime() const;

    QDateTime endTime() const;

public slots:
    void setTitle(QString arg);

    void setStartTime(QDateTime arg);

    void setEndTime(QDateTime arg);

signals:
    void titleChanged(QString arg);

    void startTimeChanged(QDateTime arg);

    void endTimeChanged(QDateTime arg);

    void changed(LpimTask&);

private:
    QScopedPointer<LpimTaskPrivate> mImpl;
    };
LPIM_END_NAMESPACE

Q_DECLARE_METATYPE(LPIM_NAMESPACE::LpimTask)

#endif // LPIMTASK_H
