#include "lpimtask.h"

#include <QDateTime>
#include <QDebug>

LPIM_BEGIN_NAMESPACE

class LpimTaskPrivate
{
public:
    LpimTaskPrivate() :
        mStartTime(QDateTime::currentDateTime()),
        mEndTime(QDateTime::currentDateTime())
    {
    }

    LpimTaskPrivate(const LpimTaskPrivate& other) :
        mStartTime(other.mStartTime),
        mEndTime(other.mEndTime),
        mTitle(other.mTitle)
    {
    }

    LpimTaskPrivate& operator=(const LpimTaskPrivate& other)
    {
        this->mStartTime = other.mStartTime;
        this->mEndTime = other.mEndTime;
        this->mTitle = other.mTitle;
        return *this;
    }

    inline int msecLength() const;
    inline void move(QDateTime newStartTime);

private:
    QDateTime mStartTime;
    QDateTime mEndTime;
    QString mTitle;

    friend class LpimTask;
};

int LpimTaskPrivate::msecLength() const
{
    int msec = mEndTime.time().msecsSinceStartOfDay() -
            mStartTime.time().msecsSinceStartOfDay();
    return msec + mStartTime.date().daysTo(mEndTime.date()) *
            QTime(24, 0).msecsSinceStartOfDay();
}

void LpimTaskPrivate::move(QDateTime newStartTime)
{
    int length = msecLength();
    mStartTime = newStartTime;
    mEndTime = mStartTime.addMSecs(length);
}

LpimTask::LpimTask(QObject *parent) :
    QObject(parent),
    mImpl(new LpimTaskPrivate())
{
}

LpimTask::LpimTask(const LpimTask &other) :
    QObject(),
    mImpl(new LpimTaskPrivate(*other.mImpl.data()))
{
}

LpimTask::~LpimTask()
{
}

LpimTask &LpimTask::operator=(const LpimTask &other)
{
    mImpl.reset(new LpimTaskPrivate(*other.mImpl.data()));
    return *this;
}

void LpimTask::move(QDateTime newStartTime)
{
    qDebug() << "LpimTask::move: from " <<
                mImpl->mStartTime << " to " << newStartTime;
    mImpl->move(newStartTime);
    emit changed(*this);
}

//const QDateTime& LpimTask::getStartTime() const
//{
//    return mImpl->mStartTime;
//}

//void LpimTask::setStartTime(const QDateTime &startTime)
//{
//    mImpl->mStartTime = startTime;
//}

//const QDateTime& LpimTask::getEndTime() const
//{
//    return mImpl->mEndTime;
//}

//void LpimTask::setEndTime(const QDateTime &endTime)
//{
//    mImpl->mEndTime = endTime;
//}

QString LpimTask::title() const
{
    return mImpl->mTitle;
}

QDateTime LpimTask::startTime() const
{
    return mImpl->mStartTime;
}

QDateTime LpimTask::endTime() const
{
    return mImpl->mEndTime;
}

void LpimTask::setTitle(QString arg)
{
    if (mImpl->mTitle != arg) {
        mImpl->mTitle = arg;
        emit titleChanged(arg);
        emit changed(*this);
    }
}

void LpimTask::setStartTime(QDateTime arg)
{
    if (mImpl->mStartTime != arg) {
        mImpl->mStartTime = arg;
        emit startTimeChanged(arg);
        emit changed(*this);
    }
}

void LpimTask::setEndTime(QDateTime arg)
{
    if (mImpl->mEndTime != arg) {
        mImpl->mEndTime = arg;
        emit endTimeChanged(arg);
        emit changed(*this);
    }
}

LPIM_END_NAMESPACE
