#ifndef LPIMSQLITETASKPROVIDER_H
#define LPIMSQLITETASKPROVIDER_H

#include "librepimdomain_global.h"
#include "lpimabstracttaskprovider.h"

LPIM_BEGIN_NAMESPACE
class LpimSqliteTaskProvider : public LpimAbstractTaskProvider
{
public:
    LpimSqliteTaskProvider(QString filename);
};
LPIM_END_NAMESPACE

#endif // LPIMSQLITETASKPROVIDER_H
