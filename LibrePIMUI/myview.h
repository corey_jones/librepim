#ifndef MYVIEW_H
#define MYVIEW_H

#include <QQuickItem>
#include <QSGGeometry>
#include <QSGFlatColorMaterial>

class MyView : public QQuickItem
{
    Q_OBJECT
public:
    explicit MyView(QQuickItem *parent = 0);
    
signals:
    
public slots:

protected:
    QSGNode* updatePaintNode(QSGNode *node, UpdatePaintNodeData *data);

private:
    QSGGeometry m_geometry;
    QSGFlatColorMaterial m_material;
};

#endif // MYVIEW_H
