# Add more folders to ship with the application, here
folder_01.source = qml/LibrePIMUI
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    taskcontroller.cpp \
    taskviewcontroller.cpp \
    myview.cpp \
    tasklayoutalgo.cpp \
    tasklayoutinfo.cpp \
    tst_tasklayoutalgo.cpp

# Installation path
# target.path =

CONFIG += LibrePIMDomain
QMAKE_CXXFLAGS += -std=c++11

# Please do not modify the following two lines. Required for deployment.
include(qtquick2applicationviewer/qtquick2applicationviewer.pri)
qtcAddDeployment()

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../LibrePIMDomain/release/ -lLibrePIMDomain
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../LibrePIMDomain/debug/ -lLibrePIMDomain
else:unix: LIBS += -L$$OUT_PWD/../LibrePIMDomain/ -lLibrePIMDomain

INCLUDEPATH += $$PWD/../LibrePIMDomain
DEPENDPATH += $$PWD/../LibrePIMDomain

HEADERS += \
    taskviewcontroller.h \
    taskcontroller.h \
    myview.h \
    tasklayoutalgo.h \
    tasklayoutinfo.h

lpimtests {
    DEFINES += LPIMTESTS
    QT += testlib
    SOURCES +=
    DEFINES += SRCDIR=\\\"$$PWD/\\\"
}

