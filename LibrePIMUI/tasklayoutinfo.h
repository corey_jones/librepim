#ifndef TASKLAYOUTINFO_H
#define TASKLAYOUTINFO_H

#include <QObject>

void simplify(int& dividend, int& divisor);

class TaskLayoutInfo : public QObject
{
    Q_OBJECT
public:
    explicit TaskLayoutInfo(int begin, int end, QObject *parent = 0);

    Q_INVOKABLE int widthDividend() const;
    Q_INVOKABLE int widthDivisor() const;
    Q_INVOKABLE int placeDividend() const;
    Q_INVOKABLE int placeDivisor() const;

    QObject *clone() const;

    inline bool operator<(const TaskLayoutInfo &that) const
    {
        return begin < that.begin;
    }

    inline bool crosses(const TaskLayoutInfo *that) const
    {
        return !(that->end < begin || end < that->begin);
    }

    inline void setWidth(int widthDividend, int widthDivisor)
    {
        myWidthDividend = widthDividend;
        myWidthDivisor = widthDivisor;
    }

    inline void setPlace(int placeDividend, int placeDivisor)
    {
        myPlaceDividend = placeDividend;
        myPlaceDivisor = placeDivisor;
    }

    inline void simplify()
    {
        ::simplify(myPlaceDividend, myPlaceDivisor);
        ::simplify(myWidthDividend, myWidthDivisor);
    }

    QString toString() const;

    int begin;
    int end;

    int myWidthDividend;
    int myWidthDivisor;
    int myPlaceDividend;
    int myPlaceDivisor;
signals:

public slots:

};

#endif // TASKLAYOUTINFO_H
