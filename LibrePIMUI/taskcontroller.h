#ifndef TASKCONTROLLER_H
#define TASKCONTROLLER_H

#include <lpimabstracttaskprovider.h>
#include <QObject>
#include <QDate>
#include <QSharedPointer>

class TaskController : public QObject
{
    Q_OBJECT
    QDate m_startDate;

    QDate m_endDate;

public:
    explicit TaskController(QObject *parent = 0);
    // TODO: ctor from Task object

    Q_PROPERTY(QDate startDate READ startDate WRITE setStartDate NOTIFY startDateChanged)
    Q_PROPERTY(QDate endDate READ endDate WRITE setEndDate NOTIFY endDateChanged)

    QDate startDate() const
    {
        return m_startDate;
    }

    QDate endDate() const
    {
        return m_endDate;
    }

    void setTaskProvider(QSharedPointer<LPim::LpimAbstractTaskProvider>);

signals:

    void startDateChanged(QDate arg);

    void endDateChanged(QDate arg);

public slots:

    void setStartDate(QDate arg)
    {
        if (m_startDate != arg) {
            m_startDate = arg;
            emit startDateChanged(arg);
        }
    }
    void setEndDate(QDate arg)
    {
        if (m_endDate != arg) {
            m_endDate = arg;
            emit endDateChanged(arg);
        }
    }

private:
    QSharedPointer<LPim::LpimAbstractTaskProvider> taskprovider_;
};

#endif // TASKCONTROLLER_H
