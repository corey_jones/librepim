#include "taskcontroller.h"

TaskController::TaskController(QObject *parent) :
    QObject(parent)
{
}

void
TaskController::setTaskProvider(
        QSharedPointer<LPim::LpimAbstractTaskProvider> taskProvider)
{
    this->taskprovider_ = taskProvider;
}
