#ifndef TASKLAYOUTALGO_H
#define TASKLAYOUTALGO_H

#include <QObject>
#include <QList>

class TaskLayoutAlgo : public QObject
{
    Q_OBJECT
public:
    explicit TaskLayoutAlgo(QObject *parent = 0);

    // we also need begin time, end time and step
    Q_INVOKABLE QList<QObject *> layout(QList<QObject *> tasks);
signals:

public slots:

};



#endif // TASKLAYOUTALGO_H
