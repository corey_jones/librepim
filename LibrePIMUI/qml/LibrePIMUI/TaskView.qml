import QtQuick 2.3
import QtQuick.Layouts 1.1

Item {
    id: __taskviewRoot
    property var model: null
    property string text: ""
    property int partsCount: 1

    property alias color : __taskviewRect.color

    onWidthChanged: {
        __taskviewRect.width = width;
        __taskviewRect.x = x;
    }
    onHeightChanged: {
        __taskviewRect.height = height;
        __taskviewRect.y = y;
    }

    Drag.active: __taskviewMouseArea.drag.active
    Drag.hotSpot.x: 0
    Drag.hotSpot.y: 0

    function starts() {
        return model.startTime;
    }
    function ends() {
        return model.endTime;
    }

    Rectangle {
        id: __taskviewRect
        radius: 10
        color: "blue"
        anchors.fill: parent

        Text {
            id: __taskviewText
            anchors.fill: parent
            text: model === null ? "" : model.title
            elide: Text.ElideRight
            wrapMode: Text.Wrap
        }

        MouseArea {
            drag.target: __taskviewRoot

            onPressed: {
//                console.log("TaskView: onPressed");
                Drag.hotSpot.x = mouse.x;
                Drag.hotSpot.y = mouse.y;
            }

            onPositionChanged: {
                // TODO:
                var dt =/*__taskviewRoot.*/Drag.target;
//                console.log('TaskView: onPositionChanged: ', dt);
//                if (dt === null) {
////                    console.log('TaskView: onPositionChanged: ', dt);
//                    Drag.active = false;
//                }
            }

            onReleased: {
//                console.log('TaskView::onReleased');
                __taskviewRoot.Drag.drop();
                var dt =__taskviewRoot.Drag.target;
                if (dt !== null)
                    console.log(typeof(dt), ' ', dt.x, ' ', dt.y);
            }

            id: __taskviewMouseArea
            anchors.fill: parent
            hoverEnabled: true

            propagateComposedEvents: true

            states: [
                State {
                    when: __taskviewMouseArea.drag.active
                    PropertyChanges {
                        target: __taskviewRoot
                        opacity: 0.5
                    }
                }

            ]
        }
    }

    function select(isSelected) {
        if (isSelected) {
            __taskviewRect.border.width = 2;
        }
        else {
            __taskviewRect.border.width = 1;
        }
    }
}
