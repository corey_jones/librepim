import QtQuick 2.3
import QtQuick.Layouts 1.1

// allday task bar
RowLayout {
    property alias columns: __alldayGrid.columns
    property alias columnsSpacing: __alldayGrid.spacing
    property real itemWidth
    property real itemHeight

    function addView(taskview) {
        if (taskview.starts().getDate() === taskview.ends().getDate()) {
            for (var i = 0; i < /*__taskGrid.*/columns; ++i) {
                var date = new Date(firstDay.getDate() + i);
                if (date.getDate() === taskview.starts().getDate()) {
                    __alldayRepeater.itemAt(i).addView(taskview);
                }
            }
        }
    }

    function clear() {
        for (var i = 0; i < __alldayRepeater.count; ++i) {
            __alldayRepeater.itemAt(i).clear();
        }
    }

    Grid {
        id: __alldayGrid
        rows: 1
        Repeater {
            id: __alldayRepeater;
            model: parent.columns
            Rectangle {
                id: __notPlannedRect
                property var tasks: []
                width: itemWidth
                height: itemHeight
                color: "yellow"

                function addView(taskView) {
                    __notPlannedRect.tasks.push(taskView);
                    for (var i = 0; i < __notPlannedRect.tasks.length; ++i) {
                        var t = __notPlannedRect.tasks[i];
                        t.height = __notPlannedRect.height / __notPlannedRect.tasks.length;
                        t.width = __notPlannedRect.width;
                        var pos = mapToItem(/*__root*/taskView.parent, 0, i * t.height);
                        t.x = pos.x;
                        t.y = pos.y;
                    }
                }

                function clear() {
                    __notPlannedRect.tasks.forEach(function (x) {
                        x.destroy(1);
                    });
                    __notPlannedRect.tasks = [];
                }

                Text {
                    text: index.toString()
                }

                DropArea {
                    anchors.fill: parent
                    onDropped: __notPlannedRect.addView(drop.source);
                }
            }
        }
    }
}
