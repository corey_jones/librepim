import QtQuick 2.3
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2

import 'testqml.js' as Code

ApplicationWindow {
//Rectangle {
    id: rectangle1
    width: 500
    height: 500

//    toolBar: ToolBar {
//        RowLayout {
//            ToolButton {
//                text: "Open"
//            }
//            ToolButton {
//                text: "Save"
//            }
//        }
//    }

    ColumnLayout {
        anchors.fill: parent

        PeriodPanel {
            id: periodPanel
            onChanged: {
                area1.gridColumns = period;
            }
        }

        TaskArea {
            controller: c

            Layout.fillHeight: true
            Layout.fillWidth: true
            id: area1
            areaWidth: parent.width
        }
    }
}

