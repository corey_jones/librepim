#if defined(LPIMTESTS)
#include <QString>
#include <QtTest>

#include <vector>
#include <memory>
#include "lpimtask.h"
#include "tasklayoutalgo.h"
#include "tasklayoutinfo.h"

inline LPim::LpimTask *make(int h0, int m0, int h1, int m1)
{
    auto t = new LPim::LpimTask();
    if (h0 >= 0 && m0 >=0 && h1 >= 0 && m1 >=0)
    {
        t->setStartTime(QDateTime(QDate::currentDate(), QTime(h0, m0)));
        t->setEndTime(QDateTime(QDate::currentDate(), QTime(h1, m1)));
    }
    else
    {
        t->setStartTime(QDateTime(QDate::currentDate(), QTime()));
        t->setEndTime(QDateTime(QDate::currentDate(), QTime()));
    }

    return t;
}

class TaskLayoutAlgoTest : public QObject
{
    Q_OBJECT

public:
    TaskLayoutAlgoTest();

private Q_SLOTS:
    void init();
    void cleanup();
    void returnsEmpty();
    void test();
private:
    QList<QObject *> args;
    QList<QObject *> result;
};

TaskLayoutAlgoTest::TaskLayoutAlgoTest()
{
}

void TaskLayoutAlgoTest::init()
{
}

void TaskLayoutAlgoTest::cleanup()
{
    for (auto a : result)
        delete a;
    result.clear();
    for (auto a : args)
        delete a;
    args.clear();
}

void TaskLayoutAlgoTest::returnsEmpty()
{
    TaskLayoutAlgo algo;
    QVERIFY2(algo.layout(QList<QObject *>()).empty(), "result is not empty");
}

#define TESTSCOUNT 14

std::vector<std::vector<int>> testData[TESTSCOUNT] =
{
        // ########
        {
            { 0, 0, 1, 0}
        },
        // ####%%%%
        {
            { 0, 0, 1, 0 },
            { 1, 0, 2, 0 }
        },
        // ..##..@
        // .%%...@
        // #######
        {
            { 0, 0, 7, 0 },
            { 1, 0, 4, 0 },
            { 2, 0, 5, 0 },
            { 6, 0, 7, 0 }
        },
        // ..###..
        // ###.@@.
        {
            { 0, 0, 3, 0 },
            { 2, 0, 5, 0 },
            { 4, 0, 6, 0 }
        },

        // ..#####....
        // .@@@.%.....
        // ####.%.....
        {
            { 0, 0, 4, 0 },
            { 1, 0, 4, 0 },
            { 2, 0, 7, 0 },
            { 5, 0, 6, 0 }
        },

        // .####....
        // @@@%%%***
        // #########
        {
            { 0, 0, 9, 0 },
            { 0, 0, 3, 0 },
            { 3, 0, 6, 0 },
            { 6, 0, 9, 0 },
            { 1, 0, 5, 0 }
        },

        // @@@%%%***
        // #########
        {
            { 0, 0, 9, 0 },
            { 0, 0, 3, 0 },
            { 3, 0, 6, 0 },
            { 6, 0, 9, 0 }
        },

        // %%..@@..**
        // ##########
        {
            { 0, 0, 10, 0 },
            { 0, 0, 2, 0 },
            { 4, 0, 6, 0 },
            { 8, 0, 10, 0 }
        },

        {
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
        },

        {
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
            { 0, 0, 2, 0 },
        },

        {
            { 0, 0, 2, 0 },
            { 1, 0, 3, 0 },
            { 2, 0, 5, 0 },
            { 2, 0, 4, 0 },
            { 4, 0, 6, 0 }
        },

        // the same as previous but has another tasks order
        {
            { 1, 0, 3, 0 },
            { 0, 0, 2, 0 },
            { 2, 0, 5, 0 },
            { 2, 0, 4, 0 },
            { 4, 0, 6, 0 }
        },

        // ..###$.
        // ###.@@.
        {
            { 0, 0, 3, 0 },
            { 2, 0, 5, 0 },
            { 4, 0, 6, 0 },
            { 5, 0, 6, 0 }
        },

        // one allday task
        {
            { -1, 0, 0, 0 }
        },
};

std::vector<std::vector<int>> testResult[TESTSCOUNT] =
{
        // ########
        {
            { 0, 1, 1, 1}
        },
        // ####%%%%
        {
            { 0, 1, 1, 1 },
            { 0, 1, 1, 1 }
        },
        // ..##..@
        // .%%...@
        // #######
        {
            { 0, 1, 1, 3 },
            { 1, 3, 1, 3 },
            { 2, 3, 1, 3 },
            { 1, 3, 2, 3 }
        },
        // ..###..
        // ###.@@.
        {
            { 1, 2, 1, 2 },
            { 0, 1, 1, 2 },
            { 1, 2, 1, 2 }
        },

        // .@@@.%.....
        // ####.%.....
        // ..#####....
        {
            { 1, 3, 1, 3 },
            { 2, 3, 1, 3 },
            { 0, 1, 1, 3 },
            { 1, 3, 2, 3 }
        },

        // @@@%%%***
        // .####.***
        // #########
        {
            { 0, 1, 1, 3 },
            { 2, 3, 1, 3 },
            { 2, 3, 1, 3 },
            { 1, 3, 2, 3 },
            { 1, 3, 1, 3 }
        },

        // @@@%%%***
        // #########
        {
            { 0, 1, 1, 2 },
            { 1, 2, 1, 2 },
            { 1, 2, 1, 2 },
            { 1, 2, 1, 2 }
        },

        // %%..@@..**
        // ##########
        {
            { 0, 1, 1, 2 },
            { 1, 2, 1, 2 },
            { 1, 2, 1, 2 },
            { 1, 2, 1, 2 }
        },

        {
            { 0, 1, 1, 10 },
            { 1, 10, 1, 10 },
            { 2, 10, 1, 10 },
            { 3, 10, 1, 10 },
            { 4, 10, 1, 10 },
            { 5, 10, 1, 10 },
            { 6, 10, 1, 10 },
            { 7, 10, 1, 10 },
            { 8, 10, 1, 10 },
            { 9, 10, 1, 10 },
        },

        {
            { 0, 1, 1, 20 },
            { 1, 20, 1, 20 },
            { 2, 20, 1, 20 },
            { 3, 20, 1, 20 },
            { 4, 20, 1, 20 },
            { 5, 20, 1, 20 },
            { 6, 20, 1, 20 },
            { 7, 20, 1, 20 },
            { 8, 20, 1, 20 },
            { 9, 20, 1, 20 },
            { 10, 20, 1, 20 },
            { 11, 20, 1, 20 },
            { 12, 20, 1, 20 },
            { 13, 20, 1, 20 },
            { 14, 20, 1, 20 },
            { 15, 20, 1, 20 },
            { 16, 20, 1, 20 },
            { 17, 20, 1, 20 },
            { 18, 20, 1, 20 },
            { 19, 20, 1, 20 },
        },

        {
            { 1, 3, 2, 3 },
            { 0, 1, 1, 3 },
            { 1, 3, 1, 3 },
            { 2, 3, 1, 3 },
            { 2, 3, 1, 3 }
        },

        {
            { 0, 1, 1, 3 },
            { 1, 3, 2, 3 },
            { 1, 3, 1, 3 },
            { 2, 3, 1, 3 },
            { 2, 3, 1, 3 }
        },

        // ..###$.
        // ###.@@.

        // result is:
        // ###..$.
        // ###.@@.
        // ..###..
        // ..###..
        {
            { 1, 2, 1, 2 },
            { 0, 1, 1, 2 },
            { 1, 2, 1, 4 },
            { 3, 4, 1, 4 }
        },

        {
            //{ 0, 0, 0, 0 },
        },
};

void TaskLayoutAlgoTest::test()
{
    for (int testcase(0); testcase < TESTSCOUNT; ++testcase)
    {
        TaskLayoutAlgo algo;
        for (auto v : testData[testcase])
        {
            args.push_back(make(v[0], v[1], v[2], v[3]));
        }

        QString message = QString("test %1: %2").arg(testcase);

        result = algo.layout(args);
        QVERIFY2(result.size() == (int)testResult[testcase].size(),
                 message.arg("result.size()").toLatin1());
        for (int i(0); i < result.size(); ++i)
        {
            TaskLayoutInfo &info = *static_cast<TaskLayoutInfo *>(result[i]);
            ::simplify(testResult[testcase][i][0], testResult[testcase][i][1]);
            ::simplify(testResult[testcase][i][2], testResult[testcase][i][3]);
            qDebug() << info.toString() << "expected: "
                        << testResult[testcase][i][0]
                        << testResult[testcase][i][1]
                        << testResult[testcase][i][2]
                        << testResult[testcase][i][3];

            QVERIFY2(info.myPlaceDividend == testResult[testcase][i][0],
                    message.arg(i).toLatin1());
            QVERIFY2(info.myPlaceDivisor == testResult[testcase][i][1],
                    message.arg(i).toLatin1());
            QVERIFY2(info.myWidthDividend == testResult[testcase][i][2],
                    message.arg(i).toLatin1());
            QVERIFY2(info.myWidthDivisor == testResult[testcase][i][3],
                    message.arg(i).toLatin1());
        }

        cleanup();
    }
}

QTEST_APPLESS_MAIN(TaskLayoutAlgoTest)

#include "tst_tasklayoutalgo.moc"
#endif

