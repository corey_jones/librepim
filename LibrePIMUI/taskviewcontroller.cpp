#include "taskviewcontroller.h"

TaskViewController::TaskViewController(QObject *parent) :
    QObject(parent)
{
}

QList<QObject *> TaskViewController::fetchTasks(QDate start, QDate end)
{
    Q_ASSERT_X(!mTaskProvider.isNull(),
               "fetchTasks", "taskprovider is not set");
    QList<QObject*> result;
    if (!mTaskProvider.isNull())
    {
        QVector<LPim::LpimTaskSharedPtr> tasks;
        mTaskProvider->fetch(
                    QDateTime(start), QDateTime(end), tasks);
        for (auto i(tasks.begin()); i != tasks.end(); ++i)
        {
            result.append((*i).data());
        }

        auto comparer = [](const QObject* x1, const QObject* x2)
        {
            auto t1 = static_cast<const LPim::LpimTask*>(x1),
                    t2 = static_cast<const LPim::LpimTask*>(x2);
            return t1->startTime() < t2->startTime();
        };

        std::sort(result.begin(), result.end(), comparer);
    }

    qDebug() << "TaskViewController::fetchTasks: " << result.size() << " fetched";
    return result;
}

void TaskViewController::selectedItem(int index)
{
    //qDebug("cpp: selected index: %d", index);
    if (index < 0)
    {
        resetSelection();
        return;
    }

    if ((size_t)index >= mSelected.size())
        mSelected.resize(index + 1);
    mSelected[index] = true;
    //qDebug("%d", index);
    //qDebug(QString("{0}").arg(index));

}

void TaskViewController::resetSelection()
{
    mSelected.assign(mSelected.size(), false);
}

bool TaskViewController::selected(int index)
{
//    qDebug("TaskViewController::selected");
    if ((size_t)index < 0 || (size_t)index >= mSelected.size())
        return false;
    return mSelected[(size_t)index];
}

void TaskViewController::addTask(QDateTime begin, QDateTime end)
{
    Q_UNUSED(begin);
    Q_UNUSED(end);

//    qDebug() << "TaskViewController::addTask 1: " <<
//                begin << " " << end;

    LPim::LpimTask *newTask = mTaskProvider->create();
    newTask->setStartTime(QDateTime(begin));
    newTask->setEndTime(QDateTime(end));

//    qDebug() << "TaskViewController::addTask 2: " <<
//                newTask->startTime() << " " <<
//                newTask->endTime();

    mTaskProvider->save();
}

QObject *TaskViewController::layoutAlgo()
{
    return new TaskLayoutAlgo(this); // qml engine takes ownership
}

void
TaskViewController::setTaskProvider(
        QSharedPointer<LPim::LpimAbstractTaskProvider> taskProvider)
{
    mTaskProvider = taskProvider;
    connect(mTaskProvider.data(), SIGNAL(changed()),
            this, SLOT(taskProviderChanged()));
}

void TaskViewController::taskProviderChanged()
{
    emit changed();
}
