#include "tasklayoutalgo.h"
#include "tasklayoutinfo.h"

#include <lpimtask.h>

#include <QDebug>

TaskLayoutAlgo::TaskLayoutAlgo(QObject *parent) :
    QObject(parent)
{
}

#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <memory>

using std::stack;
using std::vector;
using std::cout;
using std::endl;

void simplify(int& dividend, int& divisor)
{
    Q_ASSERT(dividend || divisor);
    if (!dividend && !divisor)
    {
        // dividend == 0 && divisor == 0 causes infinite loop
        return;
    }

    const int primescount = 20;
    static int prm[primescount] =
    {
      2,      3,      5,      7,     11,     13,     17,     19,     23,     29,
     31,     37,     41,     43,     47,     53,     59,     61,     67,     71
    };

    for (int i(0); i < primescount; ++i)
    {
        while (dividend % prm[i] == 0 && divisor % prm[i] == 0)
        {
            dividend /= prm[i];
            divisor /= prm[i];
        }
    }
}

typedef std::shared_ptr<TaskLayoutInfo> TaskLayoutInfoPtr;
typedef vector<TaskLayoutInfoPtr> TaskLayoutInfoArray;

int maxEnd(const TaskLayoutInfoArray &);

size_t firstToPlace(const TaskLayoutInfoArray &tasks, size_t &maxCrosses)
{
    assert(!tasks.empty());

    // O(n^2) sucks
    vector<size_t> crosses(tasks.size(), 0);
    for (size_t i(0); i < tasks.size(); ++i)
    {
        for (size_t j(0); j < tasks.size(); ++j)
        {
            if (i != j)
            {
                if (tasks[i]->crosses(tasks[j].get()))
                {
                    crosses[i]++;
                }
            }
        }
    }

    maxCrosses = crosses.front();
    size_t result(0);
    for (size_t i(1); i < crosses.size(); ++i)
    {
        if (crosses[i] > maxCrosses)
        {
            maxCrosses = crosses[i];
            result = i;
        }
    }

    crosses.assign(maxEnd(tasks) + 1, 1);
    for (size_t i(0); i < tasks.size(); ++i)
    {
        if (i != result)
        {
            for (size_t j(tasks[i]->begin); (int)j <= tasks[i]->end; ++j)
            {
                crosses[j]++;
            }
        }
    }
    maxCrosses = *std::max_element(
                crosses.begin() + tasks[result]->begin,
                crosses.begin() + tasks[result]->end);

    return result;
}

int maxEnd(const TaskLayoutInfoArray &tasks)
{
    if (tasks.empty())
    {
        return -1;
    }

    int end = (**std::max_element(
                tasks.begin(), tasks.end(),
                [](const TaskLayoutInfoPtr &ti1, const TaskLayoutInfoPtr &ti2) -> bool
                    {
                        return (*ti1).end < (*ti2).end;
                    })).end;
    return end;
}

void splitIntoGroups(const TaskLayoutInfoArray &tasks_,
                     std::vector<TaskLayoutInfoArray> &groups)
{
    TaskLayoutInfoArray tasks(tasks_.begin(), tasks_.end());
    if (tasks.empty())
    {
        return;
    }

    vector<int> ind(tasks.size(), 0);
    vector<vector<bool>> adjacency(tasks.size(),
                                   vector<bool>(tasks.size(), false));
    // O(n^2) again
    for (size_t i(0); i < tasks.size(); ++i)
    {
        for (size_t j(0); j < tasks.size(); ++j)
        {
            adjacency[i][j] = i != j && tasks[i]->crosses(tasks[j].get());
        }
    }

    int g(0);
    stack<int> nodes;
    for (size_t i(0); i < ind.size(); ++i)
    {
        if (ind[i] == 0)
        {
            ind[i] = ++g;
            nodes.push(i);
            while (!nodes.empty())
            {
                int current = nodes.top();
                nodes.pop();
                for (size_t j(0); j < adjacency.size(); ++j)
                {
                    if (adjacency[current][j] && ind[j] == 0)
                    {
                        ind[j] = ind[i];
                        nodes.push(j);
                    }
                }
            }
        }
    }

    groups.resize(g);
    for (size_t i(0); i < ind.size(); ++i)
    {
        groups[ind[i] - 1].push_back(tasks[i]);
    }
}

void layoutGroup(const TaskLayoutInfoArray &tasks_,
                 int widthDividend = 1, int widthDivisor = 1,
                 int placeDividend = 0, int placeDivisor = 1)
{
    TaskLayoutInfoArray tasks(tasks_.begin(), tasks_.end());

    if (tasks.empty())
    {
        return;
    }

    simplify(widthDividend, widthDivisor);
    simplify(placeDividend, placeDivisor);

    vector<TaskLayoutInfoArray> groups;
    splitIntoGroups(tasks, groups);
    for (auto group = groups.begin(); group != groups.end(); ++group)
    {
        size_t crosses(0);
        size_t first = firstToPlace(*group, crosses);
        TaskLayoutInfoArray::iterator eraseIt = (*group).begin() + first;

        if (crosses)
            (*eraseIt)->setWidth(widthDividend, widthDivisor * crosses);
        else
            (*eraseIt)->setWidth(widthDividend, widthDivisor);

        (*eraseIt)->setPlace(placeDividend, placeDivisor);
        (*group).erase(eraseIt);

        if (!(*group).empty() && crosses)
            layoutGroup((*group), widthDividend * (crosses - 1),
                        widthDivisor * crosses,
                        placeDividend * widthDivisor * crosses + placeDivisor * widthDividend,
                        placeDivisor * widthDivisor * crosses);
    }
}

QList<QObject *> TaskLayoutAlgo::layout(QList<QObject *> tasks)
{
    TaskLayoutInfoArray taskInfos;
    for (size_t i(0); i < (size_t)tasks.size(); ++i)
    {
        LPim::LpimTask *t = static_cast<LPim::LpimTask *>(tasks[i]);
        QDateTime start = t->startTime(),
                end = t->endTime();
        // skip if task is allday task (time() returns the same)
        if (start.time() == end.time())
        {
            continue;
        }
// TODO: 4, 15 - methods params
        int b = start.time().hour() * 4 + start.time().minute() / 15;
        int e = end.time().hour() * 4 + end.time().minute() / 15 - 1;
//        qDebug() << "TaskLayoutAlgo::layout(): "
//                    << b << " " << e;

        taskInfos.push_back(
                    std::make_shared<TaskLayoutInfo>(b, e));
    }

//    qDebug() << "TaskLayoutAlgo::layout: -- ";
//    std::for_each(tasks.begin(), tasks.end(), [](QList<QObject *>::const_reference t)
//    {
//        qDebug() << static_cast<LPim::LpimTask *>(t)->startTime() <<
//                    " " << static_cast<LPim::LpimTask *>(t)->endTime();
//    });
//    qDebug() << "-- ";

//    qDebug() << "before layoutGroup " << taskInfos.size();
    layoutGroup(taskInfos);
    QList<QObject *> result;
    std::for_each(taskInfos.begin(), taskInfos.end(),
                  [&result](TaskLayoutInfoArray::const_reference info)
    {
        info->simplify();
        result.push_back(info->clone());
    });

    return result;
}
